import pygame as pg
from random import uniform
from prey_vehicle import PreyVehicle
import boid

class Prey(PreyVehicle):

    # CONFIG
    debug = False
    min_speed = .3
    max_speed = .3
    max_force = 1
    max_turn = 10
    perception = 60
    crowding = 15
    can_wrap = False
    edge_distance_pct = 20
    ###############

    def __init__(self):
        Prey.set_boundary(Prey.edge_distance_pct)

        # Randomize starting position and velocity
        start_position = pg.math.Vector2(
            uniform(0, Prey.max_x),
            uniform(0, Prey.max_y))
        start_velocity = pg.math.Vector2(
            uniform(-1, 1) * Prey.max_speed,
            uniform(-1, 1) * Prey.max_speed)

        super().__init__(start_position, start_velocity,
                         Prey.min_speed, Prey.max_speed,
                         Prey.max_force, Prey.can_wrap)

        self.rect = self.image.get_rect(center=self.position)

        self.debug = Prey.debug

    def separation(self, preys):
        steering = pg.Vector2()
        for prey in preys:
            if isinstance(prey, Prey):
                dist = self.position.distance_to(prey.position)
                if dist < self.crowding:
                    steering -= prey.position - self.position
        steering = self.clamp_force(steering)
        return steering

    def alignment(self, preys, prey_run_away = True):

        if(prey_run_away):
            ##### change += to = to get similar but less flock-like behavior

            boids_list = [i for i in preys if isinstance(i, boid.Boid)]
            steering = pg.Vector2()

            for i, prey in enumerate(preys):

                # print(type(boid))
                if isinstance(prey, Prey):

                    distances = []

                    for boid_element in boids_list:
                        dist_to_prey = boid_element.position.distance_to(prey.position)

                        distances.append(dist_to_prey)

                    if (len(distances) is not 0):
                        closest_boid = boids_list[distances.index(min(distances))]

                        here_to_there = -1 * pg.Vector2(closest_boid.position.x - prey.position.x,
                                                   closest_boid.position.y - prey.position.y)

                        ##### change += to = to get similar but less flock-like behavior
                        steering += here_to_there
                        # steering += closest_prey.velocity * 100
                        # print("distance is 0" + str(i))

                    #steering += prey.velocity
        else:
            steering = pg.Vector2()
        steering /= len(preys)
        steering -= self.velocity
        #steering = self.clamp_force(steering)
        return steering / 8

    def cohesion(self, preys):
        steering = pg.Vector2()
        #for prey in preys:
        #     if(isinstance(prey, boid.Boid)):
        #        steering -= prey.position * 1000
        #steering /= len(preys)
        #steering -= self.position
        #steering = self.clamp_force(steering)
        return steering / 100

    def update(self, dt, preys):
        steering = pg.Vector2()

        if not self.can_wrap:
            steering += self.avoid_edge()

        neighbors = self.get_neighbors(preys)
        if neighbors:

            separation = self.separation(neighbors)
            alignment = self.alignment(neighbors)
            cohesion = self.cohesion(neighbors)

            # DEBUG
            # separation *= 0
            # alignment *= 0
            # cohesion *= 0

            steering += separation + alignment + cohesion

        # steering = self.clamp_force(steering)

        super().update(dt, steering)

    def get_neighbors(self, preys):
        neighbors = []
        for prey in preys:
            if prey != self:
                dist = self.position.distance_to(prey.position)
                #if dist < self.perception:
                neighbors.append(prey)
        return neighbors
