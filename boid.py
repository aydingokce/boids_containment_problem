import pygame as pg
from random import uniform
import math
from prey import Prey
from vehicle import Vehicle


class Boid(Vehicle):

    # CONFIG
    debug = False
    min_speed = .25
    max_speed = .25
    max_force = 1
    max_turn = 10
    perception = 60
    crowding = 15
    can_wrap = False
    edge_distance_pct = 20
    ###############

    def __init__(self):
        Boid.set_boundary(Boid.edge_distance_pct)

        # Randomize starting position and velocity
        start_position = pg.math.Vector2(
            uniform(0, Boid.max_x),
            uniform(0, Boid.max_y))
        start_velocity = pg.math.Vector2(
            uniform(-1, 1) * Boid.max_speed,
            uniform(-1, 1) * Boid.max_speed)

        super().__init__(start_position, start_velocity,
                         Boid.min_speed, Boid.max_speed,
                         Boid.max_force, Boid.can_wrap)

        self.rect = self.image.get_rect(center=self.position)

        self.debug = Boid.debug

    def separation(self, boids):
        steering = pg.Vector2()
        for boid in boids:
            if isinstance(boid, Boid):
                dist = self.position.distance_to(boid.position)
                if dist < self.crowding:
                    steering -= boid.position - self.position
        steering = self.clamp_force(steering)
        return steering

    def alignment(self, boids):

        prey_list = [i for i in boids if isinstance(i, Prey)]
        steering = pg.Vector2()

        for i, boid in enumerate(boids):

            #print(type(boid))
            if isinstance(boid, Boid):

                distances = []

                for prey_element in prey_list:

                    dist_to_boid = prey_element.position.distance_to(boid.position)


                    distances.append(dist_to_boid)

                if(len(distances) is not 0):
                    closest_prey = prey_list[distances.index(min(distances))]

                    here_to_there = pg.Vector2(closest_prey.position.x - boid.position.x, closest_prey.position.y - boid.position.y)

                    ##### change += to = to get similar but less flock-like behavior
                    steering = here_to_there
                    #steering += closest_prey.velocity * 100
                    #print("distance is 0" + str(i))

                steering += boid.velocity


        steering /= len(boids)
        steering -= self.velocity
        #steering = self.clamp_force(steering)
        return steering / 8

    def cohesion(self, boids):
        steering = pg.Vector2()
        for boid in boids:
            if (isinstance(boid, Prey)):
                pass
            else:
                steering += boid.position
        steering /= len(boids)
        steering -= self.position
        steering = self.clamp_force(steering)
        return steering / 100

    def update(self, dt, boids, multiplier = 1):
        steering = pg.Vector2()

        if not self.can_wrap:
            steering += self.avoid_edge()

        neighbors = self.get_neighbors(boids)
        if neighbors:

            separation = self.separation(neighbors)
            alignment = self.alignment(neighbors)


            cohesion = self.cohesion(neighbors)

            # DEBUG
            # separation *= 0
            # alignment *= 0
            # cohesion *= 0

            steering += separation + alignment + cohesion

        # steering = self.clamp_force(steering)

        super().update(dt, steering)

    def get_neighbors(self, boids):
        neighbors = []
        for boid in boids:

            if(isinstance(boid, Prey)):
                neighbors.append(boid)
            else:
                if boid != self:
                    dist = self.position.distance_to(boid.position)
                    if dist < self.perception:
                        neighbors.append(boid)

        return neighbors
